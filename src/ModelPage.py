#    This file is part of wos-manager.
#
#   wos-manager is free software: you can redistribute it and/or modify it under the terms of the GNU Affero Public License
#   as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
#   wos-manager is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#   without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#   See the GNU Affero Public License for more details.
#
#   You should have received a copy of the GNU Affero Public License along with wos-manager. If not, see <https://www.gnu.org/licenses/>. 

import operator
import filehelper as FileHelper
from rapidfuzz import fuzz
from PySide6.QtWidgets import QApplication, QDialog, QLineEdit, QPushButton, QVBoxLayout, QLabel, QWidget, QScrollArea, QHBoxLayout, QGridLayout, QSizePolicy, QMenu, QPlainTextEdit, QFileDialog, QMessageBox, QSpacerItem, QInputDialog
from PySide6.QtCore import Qt, Signal
from PySide6.QtGui import QIcon, QFont, QPixmap, QAction

MagicNumber = 0

#{{{ main widget that contains everything
class MainWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.Layout = QHBoxLayout(self)

        self.Sidebar = QSidebar()
        self.ModelList = QModelList()

        self.Layout.addWidget(self.ModelList)
        self.Layout.addWidget(self.Sidebar)
#}}}

#{{{ Sidebar allowing the user to change modeldata
class QSidebar(QWidget):
    def __init__(self):
        super().__init__()
        self.Layout = QVBoxLayout(self)
        self.Layout.setAlignment(Qt.AlignTop)
        self.setFixedWidth(400)
        self.hide()

        self.ModelData = None

        # close and manage button
        self.PositioningContainer = QWidget()
        self.PositioningContainerLayout = QHBoxLayout(self.PositioningContainer)
        self.PositioningContainerLayout.setAlignment(Qt.AlignRight)

        self.ManageButton = QPushButton()
        self.ManageButton.setFlat(True)
        self.ManageButton.setIcon(QIcon(str(FileHelper.SettingsIconPath)))
        self.ManageButton.setFixedSize(30, 30)


        self.CloseButton = QPushButton()
        self.CloseButton.setFlat(True)
        self.CloseButton.setIcon(QIcon(str(FileHelper.CloseIconPath)))
        self.CloseButton.setFixedSize(30, 30)
        self.CloseButton.clicked.connect(self.Close)

        self.PositioningContainerLayout.addWidget(self.CloseButton)
        self.PositioningContainerLayout.addWidget(self.ManageButton)

        # Name label
        self.NameLabel = QNameInput(self.ModelData)

        # Thumbnail
        self.ThumbnailWidget = QLabel()

        # Description label
        self.DescriptionLabel = QDescriptionInput(self.ModelData)

        # Spacer item
        self.Spacer = QSpacerItem(10, 10, QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)

        # CopyButton
        self.CopyButton = QPushButton("Copy link")
        self.CopyButton.clicked.connect(self.CopyLink)

        # OpenPlaceButton
        self.OpenButton = QPushButton("Open roblox place")
        self.OpenButton.clicked.connect(self.OpenPlace)


        # placing widgets
        self.Layout.addWidget(self.PositioningContainer)
        self.Layout.addWidget(self.NameLabel)
        self.Layout.addWidget(self.ThumbnailWidget)
        self.Layout.addWidget(self.DescriptionLabel)
        self.Layout.addItem(self.Spacer)
        self.Layout.addWidget(self.CopyButton)
        self.Layout.addWidget(self.OpenButton)

        FileHelper.AddToUpdateCallback(self.UpdateData)
    
    def SelectModel(self, ModelData):
        self.ModelData = ModelData
        self.show()
        #self.parentWidget().ModelList.DrawList()
        self.UpdateData()

    def CopyLink(self):
        Clipboard = QApplication.clipboard()
        Clipboard.setText(self.ModelData.GetLink())

    def OpenPlace(self):
        try:
            self.ModelData.OpenRobloxPlace()
        except Exception as error:
            QMessageBox.warning(self, "error", str(error))


    def UpdateData(self):
        if self.ModelData:
            self.ModelData.ImportMetadata()
            self.Image = QPixmap(self.ModelData.GetThumbnailPath())
            self.Image = self.Image.scaledToWidth(350)
            self.ThumbnailWidget.setPixmap(self.Image)
            self.ThumbnailWidget.show()

            self.NameLabel.Update(self.ModelData)
            self.DescriptionLabel.Update(self.ModelData)

            self.ManageButtonMenu = ModelDataManipulationMenu(self.ModelData)
            self.ManageButton.setMenu(self.ManageButtonMenu)

            if self.ModelData.HasSetRobloxPlace():
                self.OpenButton.setEnabled(True)
                self.OpenButton.setToolTip("")
            else:
                self.OpenButton.setEnabled(False)
                self.OpenButton.setToolTip("no roblox place found")

    def Close(self):
        global MagicNumber
        self.hide()
        self.ModelData = None

#}}}

#{{{ QModelList: Widget containing a list of all avaiable models and a search bar
class QModelList(QWidget):
    def __init__(self):
        super().__init__()
        self.Layout = QVBoxLayout(self)
        #self.setMinimumWidth(400)

        self.RowLength = 2
        self.Models = FileHelper.GetModels()
        
        # container that holds the searchbar and the button for creatintg models
        self.Container = QWidget()
        self.ContainerLayout = QHBoxLayout(self.Container)

        ## Searchbar
        self.Searchbar = QLineEdit()
        self.Searchbar.setPlaceholderText("Search")
        self.Searchbar.setClearButtonEnabled(True)
        self.Searchbar.editingFinished.connect(self.DrawList)

        ## New model button
        self.NewModelButton = QPushButton("New")
        self.NewModelButton.setIcon(QIcon(str(FileHelper.CreateIconPath)))
        self.NewModelButton.setFlat(True)
        self.NewModelButton.clicked.connect(FileHelper.CreateNewModel)

        self.ContainerLayout.addWidget(self.Searchbar)
        self.ContainerLayout.addWidget(self.NewModelButton)

        # Model list
        self.ListWidget = QWidget()
        self.ListLayout = QGridLayout(self.ListWidget)
        self.ListLayout.setAlignment(Qt.AlignTop | Qt.AlignHCenter)


        self.ScrollableArea = QScrollArea()
        self.ScrollableArea.setWidgetResizable(True)
        self.ScrollableArea.setWidget(self.ListWidget)
        
        FileHelper.AddToUpdateCallback(self.DrawList)
        
        # placing widgets
        self.Layout.addWidget(self.Container)
        self.Layout.addWidget(self.ScrollableArea)

    def DrawList(self):
        #removing all the old items
        for i in range(self.ListLayout.count()):
            self.ListLayout.takeAt(0).widget().deleteLater()

        SearchQuery = self.Searchbar.text()
        if SearchQuery:
            NewModels = FileHelper.GetModels()

            #pairing up each model with its score in tuples
            for i, v in enumerate(NewModels):
                NewModels[i] = (v, fuzz.ratio(SearchQuery, v.Name))

            #sorting the tupoles based on their score
            list.sort(NewModels, key=operator.itemgetter(1), reverse=True)

            #the each tuple is replaced with its model
            for i, v in enumerate(NewModels):
                NewModels[i] = v[0]

        else:
            NewModels = FileHelper.GetModels()

        if self.RowLength == 0:
            for i, v in enumerate(NewModels):
                Model = QSimpleModelWidget(v)
                #Model.Changed.connect(self.ModelSelected)
                self.ListLayout.addWidget(Model, i, 0)
        else:
            #creating the new items
            Column = 0
            FirstIteration = True
            for i, v in enumerate(NewModels):
                if i % self.RowLength == 0 and FirstIteration is False:
                    Column += 1

                Model = QModelWidget(v)
                Model.Changed.connect(self.ModelSelected)
                self.ListLayout.addWidget(Model, Column, i % self.RowLength)
                #print("row: {}, column: {}, {}".format(i%self.RowLength, Column, v.Name))
                FirstIteration = False

        self.ListWidget.show()

    def ModelSelected(self, ModelData):
        self.parentWidget().Sidebar.SelectModel(ModelData)

    def resizeEvent(self, event):
        NewRowLength = (self.size().width() - 75) // 300
        if NewRowLength != self.RowLength:
            self.RowLength = NewRowLength
            self.DrawList()
        super().resizeEvent(event)
#}}}

#{{{ QModelWidget: widget that displays info about a model and allows to modify it
class QModelWidget(QPushButton):
    Changed = Signal(FileHelper.ModelData)
    def __init__(self, ModelData):
        super().__init__()
        self.Layout = QVBoxLayout(self)
        self.setFixedSize(300, 350)
        self.clicked.connect(self.Selected)

        self.ModelData = ModelData

        # image
        self.ImageWidget = QLabel()

        # container
        self.Container = QWidget()
        self.ContainerLayout = QHBoxLayout(self.Container)

        self.NameLabel = QLabel()
        self.NameLabel.setAlignment(Qt.AlignCenter)

        self.NameFont = QFont()
        self.NameFont.setPointSize(16)

        self.NameLabel.setFont(self.NameFont)


        ## manage button
        self.ManageButton = QPushButton()
        self.ManageButton.setFlat(True)
        self.ManageButton.setIcon(QIcon(str(FileHelper.SettingsIconPath)))
        self.ManageButton.setFixedSize(30, 30)

        self.ManageButtonMenu = ModelDataManipulationMenu(self.ModelData)

        self.ManageButton.setMenu(self.ManageButtonMenu)

        self.ContainerLayout.addWidget(self.NameLabel)
        self.ContainerLayout.addWidget(self.ManageButton)

        # description label
        self.DescriptionLabel = QLabel()
        self.DescriptionLabel.setAlignment(Qt.AlignCenter)
        self.DescriptionLabel.setWordWrap(True)

        self.UpdateData()

        # placing widgets
        self.Layout.addWidget(self.ImageWidget)
        self.Layout.addWidget(self.Container)
        self.Layout.addWidget(self.DescriptionLabel)

    def Selected(self):
        self.Changed.emit(self.ModelData)

    def UpdateData(self):
        self.DescriptionLabel.setText(self.ModelData.Description[0:90])
        self.NameLabel.setText(self.ModelData.Name)
        self.Image = QPixmap(self.ModelData.GetThumbnailPath())
        self.Image = self.Image.scaled(275, 200, Qt.IgnoreAspectRatio)
        self.ImageWidget.setPixmap(self.Image)
        

#}}}

#{{{ QSimpleModelWidget: displays model information in a compact way
class QSimpleModelWidget(QPushButton):
    def __init__(self, ModelData):
        super().__init__(ModelData.Name)
        self.setFlat(True)

        self.clicked.connect(self.CopyLink) 

        self.ModelData = ModelData

    def CopyLink(self):
        Clipboard = QApplication.clipboard()
        Clipboard.setText(self.ModelData.GetLink())
#}}}

#{{{ QNameInput: custom widget for inputting model names
class QNameInput(QWidget):
    def __init__(self, ModelData):
        super().__init__()
        self.Layout = QHBoxLayout(self)

        self.Editing = False
        self.ModelData = ModelData

        # Name Label
        self.NameLabel = QLabel()
        self.NameLabel.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)

        # Name edit
        self.NameEdit = QLineEdit()
        self.NameEdit.returnPressed.connect(self.SetText)
        self.NameEdit.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)

        # Edit button
        self.EditButton = QPushButton()
        self.EditButton.setIcon(QIcon(str(FileHelper.CreateIconPath)))
        self.EditButton.setFlat(True)
        self.EditButton.clicked.connect(self.ToggleEditing)
        self.EditButton.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)

        #placing widgets
        self.Layout.addWidget(self.NameLabel)
        self.Layout.addWidget(self.NameEdit)
        self.Layout.addWidget(self.EditButton)

        self.ToggleEditing()
        

    def ToggleEditing(self):
        if self.Editing:
            self.NameLabel.hide()
            self.NameEdit.show()
        else:
            self.NameLabel.show()
            self.NameEdit.hide()
            self.NameEdit.setText(self.NameLabel.text())
        self.Editing = not self.Editing


    def SetText(self):
        NewName = self.NameEdit.text()
        #self.NameEdit.setText(NewName)
        self.ModelData.SetName(NewName)

    def Update(self, ModelData):
        self.ModelData = ModelData
        self.NameEdit.setText(ModelData.Name)
        self.NameLabel.setText(ModelData.Name)
#}}}

#{{{ QDescriptionInput: custom widget for inputting model names
class QDescriptionInput(QWidget):
    def __init__(self, ModelData):
        super().__init__()
        self.Layout = QHBoxLayout(self)
        self.setMaximumHeight(200)

        self.Editing = False
        self.ModelData = ModelData

        # Description Label
        self.DescriptionLabel = QLabel()
        self.DescriptionLabel.setWordWrap(True)
        #self.DescriptionLabel.setStyleSheet("background-color:red")
        self.DescriptionLabel.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)

        # Description edit
        self.DescriptionEdit = QPlainTextEdit()
        #self.DescriptionEdit.returnPressed.connect(self.SetText)
        self.DescriptionEdit.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Preferred)
        self.DescriptionEdit.hide()

        # Edit button
        self.EditButton = QPushButton()
        self.EditButton.setIcon(QIcon(str(FileHelper.CreateIconPath)))
        self.EditButton.setFlat(True)
        self.EditButton.clicked.connect(self.ToggleEditing)
        self.EditButton.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)

        #placing widgets
        self.Layout.addWidget(self.DescriptionLabel)
        self.Layout.addWidget(self.DescriptionEdit)
        self.Layout.addWidget(self.EditButton)

        

    def ToggleEditing(self):
        if self.Editing:
            self.DescriptionLabel.show()
            self.DescriptionEdit.hide()
            self.ModelData.SetDescription(self.DescriptionEdit.toPlainText())
        else:
            self.DescriptionLabel.hide()
            self.DescriptionEdit.show()
        self.Editing = not self.Editing

    def Update(self, ModelData):
        self.ModelData = ModelData
        self.DescriptionEdit.setPlainText(ModelData.Description)
        self.DescriptionLabel.setText(ModelData.Description[0:600])
#}}}

#{{{ QTextInputDialog: a dialog that allows the user to paste in the model code
class QTextInputDialog(QDialog):
    def __init__(self):
        super().__init__()
        self.Layout = QVBoxLayout(self)
        
        self.TextEditWidget = QPlainTextEdit()
        self.TextEditWidget.setPlaceholderText("paste in the model code")

        self.ConfirmButton = QPushButton("Confirm")
        self.ConfirmButton.clicked.connect(self.accept)

        self.Layout.addWidget(self.TextEditWidget)
        self.Layout.addWidget(self.ConfirmButton)
    
    def GetResults(self):
        return self.TextEditWidget.toPlainText()
#}}}

#{{{ ModelDataManipulationMenu: a menu that has options for manipulating models
class ModelDataManipulationMenu(QMenu):
    def __init__(self, ModelData):
        super().__init__()
        self.ModelData = ModelData

        self.ManageMenu = QMenu()

        self.CopyAction = QAction("Copy link")
        self.CopyAction.triggered.connect(self.CopyLink)

        self.SetModelAction = QAction("Set model code")
        self.SetModelAction.triggered.connect(self.SetModelCode)

        self.CopyModelAction = QAction("Copy model code")
        self.CopyModelAction.triggered.connect(self.CopyModelCode)

        self.OpenPlaceAction = QAction("Open roblox place")
        self.OpenPlaceAction.triggered.connect(self.ModelData.OpenRobloxPlace)

        self.SetPlaceAction = QAction("Set roblox place")
        self.SetPlaceAction.triggered.connect(self.SetRobloxPlace)

        self.SetThumbnailAction = QAction("Set thumbnail")
        self.SetThumbnailAction.triggered.connect(self.SetThumbnailPath)

        self.SetPermamentLinkAction = QAction("Set permament link")
        self.SetPermamentLinkAction.triggered.connect(self.SetPermamentLink)

        self.RenewAction = QAction("Renew link")
        self.RenewAction.triggered.connect(self.ModelData.RenewPaste)

        self.DeleteAction = QAction("Delete")
        self.DeleteAction.triggered.connect(self.DeleteModel)

        self.addAction(self.CopyAction)
        self.addAction(self.CopyModelAction)
        self.addAction(self.SetModelAction)
        self.addAction(self.OpenPlaceAction)
        self.addAction(self.SetPlaceAction)
        self.addAction(self.SetThumbnailAction)
        self.addAction(self.SetPermamentLinkAction)
        self.addAction(self.RenewAction)
        self.addAction(self.DeleteAction)
        

    def CopyLink(self):
        Clipboard = QApplication.clipboard()
        Clipboard.setText(self.ModelData.GetLink())

    def CopyModelCode(self):
        Clipboard = QApplication.clipboard()
        Clipboard.setText(self.ModelData.GetModelCode())

    def SetModelCode(self):
        ModelInputDialog = QTextInputDialog()
        if ModelInputDialog.exec():
            self.ModelData.WriteModelData(ModelInputDialog.GetResults())

    def SetThumbnailPath(self):
        NewPath = QFileDialog.getOpenFileName(self, "Chose file")[0]
        if NewPath:
            self.ModelData.SetThumbnail(NewPath)

    def SetRobloxPlace(self):
        NewPath = QFileDialog.getOpenFileName(self, "Choose file")[0]
        if NewPath:
            self.ModelData.SetRobloxPath(NewPath)

    def SetPermamentLink(self):
        Text, ExitCode = QInputDialog.getText(self, "permament link input", "enter your permament link (type nothing to remove)", ) 
        if ExitCode:
            self.ModelData.SetPermamentLink(Text)

    def DeleteModel(self):
        Confirmation = QMessageBox.warning(self, "wos-manager", "Do you want to delete this model?", QMessageBox.Yes | QMessageBox.No)
        if Confirmation == QMessageBox.Yes:
            self.ModelData.Delete()
#}}}
